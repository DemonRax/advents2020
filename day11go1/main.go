package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(occupied(input))
}

func occupied(ss []string) int {
	board := parse(ss)
	for {
		changed := false
		board, changed = seat(board)
		if !changed {
			break
		}
	}
	return seated(board)
}

func parse(ss []string) [][]*bool {
	f := false
	res := make([][]*bool, len(ss))
	for i, s := range ss {
		res[i] = make([]*bool, len(s))
		for j, c := range s {
			if c == 'L' {
				res[i][j] = &f
			}
		}
	}
	return res
}

var adjx = []int{-1, 0, 1, 1, 1, 0, -1, -1}
var adjy = []int{-1, -1, -1, 0, 1, 1, 1, 0}

func seat(board [][]*bool) ([][]*bool, bool) {
	res := make([][]*bool, len(board))
	changed := false
	for i, row := range board {
		res[i] = make([]*bool, len(row))
		for j := range row {
			c := board[i][j]
			if c == nil {
				res[i][j] = nil
				continue
			}
			field, swapped := change(board, i, j)
			res[i][j] = field
			changed = changed || swapped
		}
	}
	return res, changed
}

func change(board [][]*bool, i, j int) (*bool, bool) {
	f, t := false, true
	if *board[i][j] {
		count := 0
		for k := range adjx {
			if occupiedSeat(board, i+adjx[k], j+adjy[k]) {
				count++
			}
		}
		if count >= 4 {
			return &f, true
		}
	} else {
		for k := range adjx {
			if occupiedSeat(board, i+adjx[k], j+adjy[k]) {
				return board[i][j], false
			}
		}
		return &t, true
	}
	return board[i][j], false
}

func occupiedSeat(board [][]*bool, i,j int) bool {
	if i < 0 || i >= len(board) {
		return false
	}
	if j< 0 || j >= len(board[0]) {
		return false
	}
	if board[i][j] == nil || !*board[i][j] {
		return false
	}
	return true
}

func seated(board [][]*bool) int {
	count := 0
	for i, row := range board {
		for j := range row {
			c := board[i][j]
			if c != nil && *c {
				count++
			}
		}
	}
	return count
}
