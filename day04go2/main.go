package main

import (
	"fmt"
	"strings"

	"gitlab.com/DemonRax/advents2020/util"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(passports(input))
}

func passports(ss []string) int {
	count := 0
	passport := map[string]string{}
	for _, s := range ss {
		if strings.TrimSpace(s) == "" {
			if util.NewPassport(passport).Valid() {
				count++
			}
			passport = map[string]string{}
			continue
		}
		fields := strings.Split(s, " ")
		for _, f := range fields {
			entry := strings.Split(f, ":")
			passport[entry[0]] = entry[1]
		}
	}
	if util.NewPassport(passport).Valid() {
		count++
	}
	return count
}