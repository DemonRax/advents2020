package main

import (
	"testing"

	"gitlab.com/DemonRax/advents2020/util"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in:   `1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc`,
			want: 1,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := check(util.ReadString(test.in)); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
