package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(check(input))
}

func check(ss []string) int {
	correct := 0
	for _, s := range ss {
		a, b, letter, password := parse(s)
		if (password[a-1] == letter) != (password[b-1] == letter) {
			correct++
		}
	}
	return correct
}

func parse(s string) (int, int, byte, string) {
	pwd := strings.Split(s, ": ")
	ltr := strings.Split(pwd[0], " ")
	nums := strings.Split(ltr[0], "-")
	a, _ := strconv.Atoi(nums[0])
	b, _ := strconv.Atoi(nums[1])
	return a, b, []byte(ltr[1])[0], pwd[1]
}
