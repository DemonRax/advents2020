package main

import (
	"strconv"
	"testing"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{in: "BFFFBBFRRR", want: 567},
		{in: "FFFBBBFRRR", want: 119},
		{in: "BBFFBBFRLL", want: 820},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := id(test.in); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}

func Test_cut(t *testing.T) {
	for _, test := range []struct{
	    a, b int
	    c rune
	    wantA, wantB int
	}{
		{
			a:     0,
			b:     127,
			c:     'F',
			wantA: 0,
			wantB: 63,
		},
		{
			a:     0,
			b:     127,
			c:     'B',
			wantA: 64,
			wantB: 127,
		},
		{
			a:     0,
			b:     63,
			c:     'F',
			wantA: 0,
			wantB: 31,
		},
		{
			a:     0,
			b:     63,
			c:     'B',
			wantA: 32,
			wantB: 63,
		},
		{
			a:     32,
			b:     63,
			c:     'L',
			wantA: 32,
			wantB: 47,
		},
		{
			a:     32,
			b:     63,
			c:     'R',
			wantA: 48,
			wantB: 63,
		},
		{
			a:     40,
			b:     47,
			c:     'L',
			wantA: 40,
			wantB: 43,
		},
		{
			a:     40,
			b:     47,
			c:     'R',
			wantA: 44,
			wantB: 47,
		},
		{
			a:     44,
			b:     45,
			c:     'L',
			wantA: 44,
			wantB: 44,
		},
		{
			a:     44,
			b:     45,
			c:     'R',
			wantA: 45,
			wantB: 45,
		},
	} {
	    t.Run(strconv.Itoa(test.a) + strconv.Itoa(test.b) + string(test.c), func(t *testing.T){
	        gotA, gotB := cut(test.a, test.b, test.c)
	        if gotA != test.wantA {
	        	t.Errorf("want A %d, got %d", test.wantA, gotA)
			}
	        if gotB != test.wantB {
	        	t.Errorf("want B %d, got %d", test.wantB, gotB)
			}
	    })
	}
}