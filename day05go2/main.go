package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2019/util"
	"sort"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(highest(input))
}

func highest(ss []string) int {
	ids := make([]int, len(ss))
	for i, s := range ss {
		ids[i] = id(s)
	}
	sort.Ints(ids)
	return missing(ids)[0]
}

func missing(ids []int) []int {
	miss := make([]int, 0, len(ids))
	for i := 1; i < len(ids); i++ {
		if next := ids[i-1] + 1; next != ids[i] {
			miss = append(miss, next)
		}
	}
	return miss
}

func id(s string) int {
	row := find(s[:7], 128)
	col := find(s[7:], 8)
	return row * 8 + col
}

const (
	f rune = 'F'
	b rune = 'B'
	l rune = 'L'
	r rune = 'R'
)

func find(s string, length int) int {
	x, y := 0, length-1
	for _, c := range s {
		x, y = cut(x, y, c)
	}
	if x != y {
		return 0
	}
	return x
}

func cut(x, y int, c rune) (int, int) {
	switch c {
	case f, l:
		y = x + (y - 1 - x)/2
	case b, r:
		x = x + (y + 1 - x)/2
	}
	return x, y
}
