package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2019/util"
	"sort"
	"strconv"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(weakness(input, 25))
}

func weakness(ss []string, pre int) int {
	nn := make([]int, len(ss))
	for i, s := range ss {
		n, _ := strconv.Atoi(s)
		nn[i] = n
	}

	invalid := find(nn, pre)
	fmt.Println(invalid)
	set := set(nn, invalid)
	sort.Ints(set)
	fmt.Println(set)
	return set[0] + set[len(set)-1]
}

func set(nn []int, sum int) []int {
	for i := range nn {
		run := nn[i]
		for j := i+1; j <= len(nn); j++ {
			run += nn[j]
			if run == sum {
				return nn[i:j+1]
			}
			if run > sum {
				break
			}
		}
	}
	return nn
}

func find(nn []int, pre int) int {
	for i, n := range nn[pre:] {
		if !check(nn[i:pre+i], n) {
			return n
		}
	}
	return 0
}

func check(nn []int, sum int) bool {
	for i, n := range nn {
		for _, m := range nn[i+1:] {
			if n+m == sum {
				return true
			}
		}
	}
	return false
}

