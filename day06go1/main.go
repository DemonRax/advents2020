package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2019/util"
	"strings"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(answers(input))
}

func answers(ss []string) int {
	count := 0
	yes := map[rune]interface{}{}
	for _, s := range ss {
		if strings.TrimSpace(s) == "" {
			count += len(yes)
			yes = map[rune]interface{}{}
			continue
		}
		for _, c := range s {
			yes[c] = struct{}{}
		}
	}
	return count + len(yes)
}
