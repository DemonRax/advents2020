package main

import (
	"gitlab.com/DemonRax/advents2020/util"
	"testing"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in: `abc

a
b
c

ab
ac

a
a
a
a

b
`,
			want: 11,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := answers(util.ReadString(test.in)); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
