package util

import (
	"strconv"
	"strings"
)

type birthYear int

func (y *birthYear) valid() bool {
	return y != nil && *y >= 1920 && *y <= 2002
}

type issueYear int

func (y *issueYear) valid() bool {
	return y != nil && *y >= 2010 && *y <= 2020
}

type expirationYear int

func (y *expirationYear) valid() bool {
	return y != nil && *y >= 2020 && *y <= 2030
}

type ht string

var (
	cm, in ht = "cm", "in"
)

type height struct {
	value int
	typ   ht
}

func (h *height) valid() bool {
	if h == nil {
		return false
	}
	if h.typ == cm {
		return h.value >= 150 && h.value <= 193
	}
	if h.typ == in {
		return h.value >= 59 && h.value <= 76
	}
	return false
}

type rgb string
func (r *rgb) valid() bool {
	if r == nil || len(*r) != 7 {
		return false
	}
	s := string(*r)
	if s[0] != '#' {
		return false
	}
	for _, c := range s[1:] {
		found := false
		for _, h := range "0123456789abcdef" {
			if c == h {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	return true
}

type id string
func (id *id) valid() bool {
	if id == nil || len(*id) != 9 {
		return false
	}
	_, err := strconv.Atoi(strings.TrimLeft(string(*id), "0"))
	return err == nil
}

type eyeColor string
var (
	amb, blu, brn, gry, grn, hzl, oth eyeColor = "amb", "blu", "brn", "gry", "grn", "hzl", "oth"
	validColors                                = []eyeColor{amb, blu, brn, gry, grn, hzl, oth}
)
func (ec eyeColor) valid() bool {
	for _, color := range validColors {
		if ec == color {
			return true
		}
	}
	return false
}

func NewPassport(data map[string]string) Passport {
	p := Passport{}
	for k, v := range data {
		p = p.Parse(k, v)
	}
	return p
}

type Passport struct {
	birthYear      birthYear
	issueYear      issueYear
	expirationYear expirationYear
	height         height
	hairColor      rgb
	eyeColor       eyeColor
	passportID     id
	countryID      string
}

func (p Passport) Valid() bool {
	return p.birthYear.valid() &&
		p.issueYear.valid() &&
		p.expirationYear.valid() &&
		p.height.valid() &&
		p.hairColor.valid() &&
		p.eyeColor.valid() &&
		p.passportID.valid()
}

func (p Passport) Parse(key, value string) Passport {
	switch key {
	case "byr":
		y, _ := strconv.Atoi(value)
		p.birthYear = birthYear(y)
	case "iyr":
		y, _ := strconv.Atoi(value)
		p.issueYear = issueYear(y)
	case "eyr":
		y, _ := strconv.Atoi(value)
		p.expirationYear = expirationYear(y)
	case "hgt":
		p.height = parseHeight(value)
	case "hcl":
		p.hairColor = rgb(value)
	case "ecl":
		p.eyeColor = eyeColor(value)
	case "pid":
		p.passportID = id(value)
	case "cid":
		p.countryID = value
	default:
	}
	return p
}

func parseHeight(value string) height {
	if len(value) < 4 {
		return height{}
	}
	typ := ht(value[len(value)-2:])
	if typ != cm && typ != in {
		return height{}
	}
	v, _ := strconv.Atoi(value[:len(value)-2])
	return height{
		value: v,
		typ:   typ,
	}
}
