package util

import "testing"

func TestName(t *testing.T) {
	for _, test := range []struct{
	    desc string
	    change map[string]string
	    want bool
	}{
	    {
	    	change: map[string]string{},
	    	want: true,
		},
	    {
	    	change: map[string]string{"byr": "1919"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"byr": "1920"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"byr": "2002"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"byr": "2003"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"iyr": "2009"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"iyr": "2010"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"iyr": "2020"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"iyr": "2021"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"eyr": "2019"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"eyr": "2020"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"eyr": "2030"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"eyr": "2031"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"hgt": "150"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"hgt": "150km"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"hgt": "123cm"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"hgt": "150cm"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"hgt": "193cm"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"hgt": "194cm"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"hgt": "59in"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"hgt": "76in"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"hgt": "77in"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"hcl": "#12345g"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"hcl": "#1234567"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"hcl": "123456"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"hcl": "#123456"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"hcl": "#abcdef"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"ecl": "amb"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"ecl": "anb"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"pid": "123456789"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"pid": "12345678z"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"pid": "12345678"},
	    	want: false,
		},
	    {
	    	change: map[string]string{"pid": "003456789"},
	    	want: true,
		},
	    {
	    	change: map[string]string{"pid": "00345678"},
	    	want: false,
		},
	} {
	    t.Run(test.desc, func(t *testing.T){
	    	valid := map[string]string{
				"byr": "1980",
				"iyr": "2015",
				"eyr": "2025",
				"hgt": "180cm",
				"hcl": "#623a2f",
				"ecl": "grn",
				"pid": "087499704",
				"cid": "none",
			}
	    	pass := NewPassport(valid)
			for k, v := range test.change {
				pass = pass.Parse(k, v)
			}
	        if got := pass.Valid(); got != test.want {
	        	t.Errorf("want valid %t, got %t", test.want, got)
			}
	    })
	}
}
