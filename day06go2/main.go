package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2019/util"
	"strings"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(answers(input))
}

func answers(ss []string) int {
	all := 0
	count := 0
	yes := map[rune]int{}
	for _, s := range ss {
		if strings.TrimSpace(s) == "" {
			all += everyone(yes, count)
			yes = map[rune]int{}
			count = 0
			continue
		}
		for _, c := range s {
			yes[c] = yes[c] + 1
		}
		count++
	}
	return all + len(yes)
}

func everyone(yes map[rune]int, count int) int {
	result := 0
	for _, i := range yes {
		if i == count {
			result++
		}
	}
	return result
}
