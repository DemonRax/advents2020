package main

import (
	"fmt"
	"strings"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(passports(input))
}

func passports(ss []string) int {
	count := 0
	passport := map[string]string{}
	for _, s := range ss {
		if strings.TrimSpace(s) == "" {
			count += valid(passport)
			passport = map[string]string{}
			continue
		}
		fields := strings.Split(s, " ")
		for _, f := range fields {
			entry := strings.Split(f, ":")
			passport[entry[0]] = entry[1]
		}
	}
	return count + valid(passport)
}

func valid(pass map[string]string) int {
	if len(pass) == 8 {
		return 1
	}
	if len(pass) == 7 {
		if _, ok := pass["cid"]; !ok {
			return 1
		}
	}
	return 0
}
