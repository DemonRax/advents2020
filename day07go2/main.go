package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(bags(input))
}

func bags(ss []string) int {
	n := 0
	count("shiny gold", scan(ss), &n, 1)
	return n
}

type bag struct {
	name string
	count int
}

func scan(ss []string) map[string][]bag {
	res := map[string][]bag{}
	for _, s := range ss {
		contain := strings.Split(s, " bags contain ")
		if contain[1] == "no other bags." {
			continue
		}
		inner := strings.Split(strings.TrimRight(contain[1], "."), ", ")
		for _, in := range inner {
			bagFields := strings.Split(in, " ")
			n, _ := strconv.Atoi(bagFields[0])
			bg := bag{
				name: bagFields[1] + " " + bagFields[2],
				count: n,
			}
			res[contain[0]] = append(res[contain[0]], bg)
		}
	}
	return res
}

func count(key string, all map[string][]bag, n *int, mul int) {
	for _, bg := range all[key] {
		*n += bg.count * mul
		count(bg.name, all, n, bg.count * mul)
	}
}