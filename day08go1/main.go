package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2019/util"
	"strconv"
	"strings"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(exec(input))
}

func exec(ss []string) int {
	acc := 0
	i := 0
	for {
		if ss[i] == "" {
			break
		}
		add := 0
		prev := i
		add, i = get(i, ss[i])
		acc += add
		ss[prev] = ""
	}
	return acc
}

func get(i int, s string) (int, int) {
	ss := strings.Split(s, " ")
	a, _ := strconv.Atoi(ss[1])
	switch ss[0] {
	case "jmp":
		return 0, i+a
	case "acc":
		return a, i+1
	}
	return 0, i+1
}