package main

import (
	"fmt"

	"gitlab.com/DemonRax/advents2020/util"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(trees(input))
}

const (
	tree = '#'
	dx   = 3
)

func trees(ss []string) int {
	var count, x int
	size := len(ss[0])
	for i := 0; i < len(ss)-1; i++ {
		x += dx
		if x > size-1 {
			x -= size
		}
		if ss[i+1][x] == tree {
			count++
		}
	}
	return count
}