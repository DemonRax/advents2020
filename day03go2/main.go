package main

import (
	"fmt"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(trees(input))
}

const (
	tree = '#'
)

func trees(ss []string) int {
	result := 1
	for _, data := range []struct {
		dx, dy int
	}{
		{1, 1},
		{3, 1},
		{5, 1},
		{7, 1},
		{1, 2},
	} {
		result *= slope(data.dx, data.dy, ss)
	}
	return result
}

func slope(dx, dy int, ss []string) int {
	var count, x int
	size := len(ss[0])
	for i := 0; i < len(ss)-dy; i += dy {
		x += dx
		if x > size-1 {
			x -= size
		}
		if ss[i+dy][x] == tree {
			count++
		}
	}
	return count
}
