package main

import (
	"testing"

	"gitlab.com/DemonRax/advents2020/util"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in:   `1721
979
366
299
675
1456`,
			want: 241861950,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := fix(util.ReadString(test.in)); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
