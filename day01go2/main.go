package main

import (
	"fmt"
	"strconv"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(fix(input))
}

func fix(ss []string) int {
	for i, s := range ss {
		a, _ := strconv.Atoi(s)
		for j, t := range ss[i:] {
			b, _ := strconv.Atoi(t)
			for _, p := range ss[j:] {
				c, _ := strconv.Atoi(p)
				if a+b+c == 2020 {
					return a*b*c
				}
			}
		}
	}
	return 0
}