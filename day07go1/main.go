package main

import (
	"fmt"
	"strings"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(bags(input))
}

func bags(ss []string) int {
	res := map[string]interface{}{}
	getColors([]string{"shiny gold"}, scan(ss), res)
	return len(res) - 1
}

func scan(ss []string) map[string][]string {
	res := map[string][]string{}
	for _, s := range ss {
		contain := strings.Split(s, " bags contain ")
		if contain[1] == "no other bags." {
			continue
		}
		inner := strings.Split(strings.TrimRight(contain[1], "."), ", ")
		for _, in := range inner {
			bag := strings.Split(in, " ")
			name := bag[1] + " " + bag[2]
			res[name] = append(res[name], contain[0])
		}
	}
	return res
}

func getColors(keys []string, bags map[string][]string, colors map[string]interface{}) {
	for _, key := range keys {
		values := bags[key]
		colors[key] = struct {}{}
		getColors(values, bags, colors)
	}
}