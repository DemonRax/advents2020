package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2019/util"
	"sort"
	"strconv"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(jolts(input, 1)*jolts(input, 3))
}

func jolts(ss []string, diff int) int {
	nn := make([]int, len(ss))
	for i, s := range ss {
		n, _ := strconv.Atoi(s)
		nn[i] = n
	}

	sort.Ints(nn)

	set := map[int]int{}

	prev := 0
	for i := 0; i < len(nn); i++ {
		x := nn[i] - prev
		set[x] = set[x]+1
		prev = nn[i]
	}

	set[3] = set[3]+1

	return set[diff]
}
