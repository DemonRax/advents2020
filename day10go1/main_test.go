package main

import (
	"gitlab.com/DemonRax/advents2020/util"
	"testing"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want1 int
		want3 int
	}{
		{
			in: `16
10
15
5
1
11
7
19
6
12
4`,
			want1: 7,
			want3: 5,
		},
		{
			in: `28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3`,
			want1: 22,
			want3: 10,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := jolts(util.ReadString(test.in), 1); got != test.want1 {
				t.Errorf("got = %d, want %d", got, test.want1)
			}
			if got := jolts(util.ReadString(test.in), 3); got != test.want3 {
				t.Errorf("got = %d, want %d", got, test.want3)
			}
		})
	}
}
