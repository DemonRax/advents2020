package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2019/util"
	"strconv"
	"strings"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(exec(input))
}

func exec(ss []string) int {
	acc := 0
	ok := false
	for i := range ss {
		acc, ok = try(ss, i)
		if ok {
			break
		}
	}
	return acc
}

func try(in []string, change int) (int, bool) {
	acc := 0
	i := 0
	correct := false

	ss := make([]string, len(in))
	for i, s := range in {
		ss[i] = s
		if i == change {
			switch {
			case strings.Contains(ss[change], "jmp"):
				tmp := strings.Split(ss[change], " ")
				ss[change] = fmt.Sprintf("%s %s", "nop", tmp[1])
			case strings.Contains(ss[change], "nop"):
				tmp := strings.Split(ss[change], " ")
				ss[change] = fmt.Sprintf("%s %s", "jmp", tmp[1])
			}
		}
	}

	for {
		if i >= len(ss) {
			correct = true
			break
		}
		if ss[i] == "" {
			break
		}
		add := 0
		prev := i
		add, i = get(i, ss[i])
		if i == prev {
			break
		}
		acc += add
		ss[prev] = ""
	}
	return acc, correct
}

func get(i int, s string) (int, int) {
	ss := strings.Split(s, " ")
	a, _ := strconv.Atoi(ss[1])
	switch ss[0] {
	case "jmp":
		return 0, i + a
	case "acc":
		return a, i + 1
	}
	return 0, i + 1
}
