package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2019/util"
	"sort"
	"strconv"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(arrangements(input))
}

func arrangements(ss []string) int {
	nn := make([]int, len(ss))
	for i, s := range ss {
		n, _ := strconv.Atoi(s)
		nn[i] = n
	}

	sort.Ints(nn)

	prev := 0
	count := 0
	res := 1
	for i := 0; i < len(nn); i++ {
		x := nn[i] - prev
		if x > 2 {
			res *= combos(count)
			count = 0
		} else {
			count++
		}
		prev = nn[i]
	}
	fmt.Println(len(mem))
	return res * combos(count)
}

var mem = map[int]int{
	0: 1,
	1: 1,
	2: 2,
	3: 4,
}

func combos(size int) int {
	if m, ok := mem[size]; ok {
		return m
	}
	res := combos(size-1) + combos(size-2) + combos(size-3)
	mem[size] = res
	return res
}
