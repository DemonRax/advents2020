package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(highest(input))
}

func highest(ss []string) int {
	highest := 0
	for _, s := range ss {
		if id := id(s); id > highest {
			highest = id
		}

	}
	return highest
}

func id(s string) int {
	row := find(s[:7], 128)
	col := find(s[7:], 8)
	return row * 8 + col
}

const (
	f rune = 'F'
	b rune = 'B'
	l rune = 'L'
	r rune = 'R'
)

func find(s string, length int) int {
	x, y := 0, length-1
	for _, c := range s {
		x, y = cut(x, y, c)
	}
	if x != y {
		return 0
	}
	return x
}

func cut(x, y int, c rune) (int, int) {
	switch c {
	case f, l:
		y = x + (y - 1 - x)/2
	case b, r:
		x = x + (y + 1 - x)/2
	}
	return x, y
}
