package main

import (
	"fmt"
	"strconv"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(fix(input))
}

func fix(ss []string) int {
	for i, s := range ss {
		a, _ := strconv.Atoi(s)
		for _, t := range ss[i:] {
			b, _ := strconv.Atoi(t)
			if a+b == 2020 {
				return a*b
			}
		}
	}
	return 0
}