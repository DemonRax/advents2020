package main

import (
	"gitlab.com/DemonRax/advents2020/util"
	"testing"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in: `35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576`,
			want: 127,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := find(util.ReadString(test.in), 5); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
