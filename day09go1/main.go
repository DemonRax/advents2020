package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2019/util"
	"strconv"
)

func main() {
	input := util.ReadFile("./input.txt")
	fmt.Println(find(input, 25))
}

func find(ss []string, pre int) int {
	nn := make([]int, len(ss))
	for i, s := range ss {
		n, _ := strconv.Atoi(s)
		nn[i] = n
	}

	for i, n := range nn[pre:] {
		if !check(nn[i:pre+i], n) {
			return n
		}
	}
	return 0
}

func check(nn []int, sum int) bool {
	for i, n := range nn {
		for _, m := range nn[i+1:] {
			if n+m == sum {
				return true
			}
		}
	}
	return false
}

